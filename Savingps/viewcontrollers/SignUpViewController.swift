//
//  SignUpViewController.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var groupsTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    var registerReponse :RegisterResponse!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func loginButtonClick(_ sender: Any) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
               self.navigationController?.pushViewController(signUpVC, animated: true)
        
    }
    
    @IBAction func createAccountTap(_ sender: Any) {
        
        let msg = validation(email: emailTextField.text!, username: userNameTextField.text!)
        if msg == "" {
            loginAPI(username: userNameTextField.text!, email: emailTextField.text!)
        }
        else{
            self.showAlert(with: msg)
        }
        
        
        
    }
    func validation(email: String,username:String) -> String {
        if username == ""{
            return "Username is empty"
        }
        else if email == "" {
            return "Email is empty"
        }
        else if !isValidEmail(email){
            return "Invaild email"
        }
        else{
            return ""
        }
    }
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func loginAPI(username: String,email: String) {
        self.showActivity()
        let params: Parameters = ["username":username,"email":email,"groups":[]]
        Alamofire.request("https://djangooncloudrun-5e57zd7bgq-ew.a.run.app/users/" , method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON { (response) in
            if let data = response.data {
                self.hideActivity()
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    print("Response: \(json)")
                    if let data = Mapper<RegisterResponse>().map(JSONString: json){
                        self.registerReponse = data
                        if let data = self.registerReponse.username {
                         
                            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "BankListViewController") as! BankListViewController
                            
                         
                            
                                   self.navigationController?.pushViewController(signUpVC, animated: true)
                               self.showAlert(with: "User Created successfully")
                        }else{
                            self.showAlert(with: "Username already exist")
                        }
                    }
                    else{
                        print("false")
                    }
                }
                
            }
        }
    }
   
}
