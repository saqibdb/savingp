//
//  HomeViewController.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var paymentLimitView: UIView!
    @IBOutlet weak var managePayeeView: UIView!
    @IBOutlet weak var payContactsView: UIView!
    @IBOutlet weak var makePaymentView: UIView!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var bottomView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bottomView.layer.cornerRadius = 25
        bottomView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        upperView.layer.cornerRadius = 25
        upperView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        shadowSetting(view: makePaymentView)
        shadowSetting(view: payContactsView)
        shadowSetting(view: managePayeeView)
        shadowSetting(view: paymentLimitView)
        let payContact = UITapGestureRecognizer(target: self, action: #selector(self.payContactsTap(_:)))
        payContactsView.addGestureRecognizer(payContact)
        let makePayment = UITapGestureRecognizer(target: self, action: #selector(self.makePaymentTap(_:)))
        makePaymentView.addGestureRecognizer(makePayment)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidLayoutSubviews() {
        scanButton.layer.cornerRadius = scanButton.frame.height / 2
    }
    func shadowSetting(view: UIView) {
        view.layer.cornerRadius = view.frame.height / 4
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 4
    }
    @objc func payContactsTap(_ sender: UITapGestureRecognizer? = nil) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "PayeeListViewController") as! PayeeListViewController
        self.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    @objc func makePaymentTap(_ sender: UITapGestureRecognizer? = nil) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "sendPaymentViewController") as! sendPaymentViewController
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    
    @IBAction func carAction(_ sender: UIButton) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "BreakdownDetailsViewController") as! BreakdownDetailsViewController
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @IBAction func savingAction(_ sender: Any) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "BreakdownDetailsViewController") as! BreakdownDetailsViewController
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @IBAction func holidayAction(_ sender: Any) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "BreakdownDetailsViewController") as! BreakdownDetailsViewController
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @IBAction func breakDownAction(_ sender: Any) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "BreakdownViewController") as! BreakdownViewController
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    
    
}
