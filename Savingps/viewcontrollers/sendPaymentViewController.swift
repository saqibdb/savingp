//
//  sendPaymentViewController.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import UIKit

class sendPaymentViewController: UIViewController {
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var bottomView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.layer.cornerRadius = 25
        bottomView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        upperView.layer.cornerRadius = 25
        upperView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        // Do any additional setup after loading the view.
    }
    

    @IBAction func continueButtonClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentConfirmationViewController") as! PaymentConfirmationViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
