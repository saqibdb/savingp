//
//  PayeeListViewController.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import UIKit

class PayeeListViewController: UIViewController {
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var newPayeeView: UIView!
    @IBOutlet weak var payeeListTableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.layer.cornerRadius = 25
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
        shadowSetting(view: searchView)
        payeeListTableView.delegate = self
        payeeListTableView.dataSource = self
        let newPayee = UITapGestureRecognizer(target: self, action: #selector(self.newPayeeTap(_:)))
        newPayeeView.addGestureRecognizer(newPayee)

        // Do any additional setup after loading the view.
    }
    @IBAction func backButtonClick(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    func shadowSetting(view: UIView) {
        view.layer.cornerRadius = view.frame.height / 4
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 4
    }
    @objc func newPayeeTap(_ sender: UITapGestureRecognizer? = nil) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewPayeeViewController") as! NewPayeeViewController
      
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension PayeeListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = payeeListTableView.dequeueReusableCell(withIdentifier: "PayeeListTableViewCell") as! PayeeListTableViewCell
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        return cell
    }
    
    
}
