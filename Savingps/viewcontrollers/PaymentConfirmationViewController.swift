//
//  PaymentConfirmationViewController.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import UIKit

class PaymentConfirmationViewController: UIViewController {
    @IBOutlet weak var bottomView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.layer.cornerRadius = 25
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]

        // Do any additional setup after loading the view.
    }
    

    @IBAction func backButtonClick(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func payButtonClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentCompleteViewController") as! PaymentCompleteViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
