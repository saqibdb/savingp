//
//  NewPayeeViewController.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import UIKit

class NewPayeeViewController: UIViewController {

    @IBOutlet weak var bottomView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.layer.cornerRadius = 25
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]

     
    }
    @IBAction func backButtonClick(_ sender: Any) {
          _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewPayeeWarningViewController") as! NewPayeeWarningViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    


}
