//
//  HomeTabBarController.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.unselectedItemTintColor = Color.tab_unselected_color
        self.tabBar.tintColor = Color.selected_color
        self.tabBar.barTintColor = Color.barBackground
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
   
        // Do any additional setup after loading the view.
    }
    
}
