//
//  BreakdownDetailsViewController.swift
//  Savingp's
//
//  Created by MacBook Pro on 09/01/2020.
//  Copyright © 2020 Vivek Dogra. All rights reserved.
//

import UIKit

class BreakdownDetailsViewController: UIViewController {
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var bottomView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.layer.cornerRadius = 25
        bottomView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        //upperView.layer.cornerRadius = 25
        //upperView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        self.view.layoutIfNeeded()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
