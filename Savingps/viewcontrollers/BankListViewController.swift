//
//  BankListViewController.swift
//  Savingps
//
//  Created by Vivek Dogra on 30/01/20.
//  Copyright © 2020 Vivek Dogra. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import Kingfisher
import WebKit
import PocketSVG

class BankListViewController: UIViewController, WKNavigationDelegate  {

    
    
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var webView: WKWebView!
    var transactionResponse = AccountTransactionResponseModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
    
                let url = URL(string: "https://auth.truelayer.com/?response_type=code&client_id=savingps-e3e3d9&scope=info%20accounts%20balance%20cards%20transactions%20direct_debits%20standing_orders%20offline_access&redirect_uri=https://console.truelayer.com/redirect-page&providers=uk-ob-all%20uk-oauth-all")!
                   let requestObj = URLRequest(url: url)
                webView.load(requestObj)
             
        self.activityIndicator.startAnimating()
        // Do any additional setup after loading the view.
    }
    @IBAction func backButtonTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)

    }
   
    func getParameterFrom(url: String, param: String) -> String? {
           guard let url = URLComponents(string: url) else {
               
               return nil }
           return url.queryItems?.first(where: { $0.name == param })?.value
       }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        let url = webView.url
        print(url as Any) // this will print url address as option field
        if url?.absoluteString.range(of: ".pdf") != nil {
           //  pdfBackButton.isHidden = false
             print("PDF contain")
        }
        else {
            // pdfBackButton.isHidden = true
             print("No PDF Contain")
        }
        
        
        if let currentURL = webView.url?.absoluteString{
                       print(currentURL)
                       if (currentURL.range(of: "https://console.truelayer.com/redirect-page?code=") != nil) {
                           var chargeId = ""
                           if let charge = self.getParameterFrom(url: currentURL, param: "code"){
                               chargeId = charge
                            UserDefaults.standard.set(chargeId, forKey: "token")
                            createTokenAPI()
                            webView.isHidden = true
                           }
                            
                         // _ = navigationController?.popViewController(animated: true)

                            
                           //NotificationCenter.default.post(name: NSNotification.Name("PaymentDetail"), object: chargeId)
                       }
                   }
    }
    
    func createTokenAPI() {
              self.showActivity()
           let params: Parameters = ["grant_type":"authorization_code","client_id":"savingps-e3e3d9","client_secret":"45894c7e-2a21-45cc-a099-5b14b8bd6527","redirect_uri":"https://console.truelayer-sandbox.com/redirect-page","code":UserDefaults.standard.string(forKey: "token") ?? ""]
              Alamofire.request("https://auth.truelayer.com/connect/token" , method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON { (response) in
                  if let data = response.data {
                      self.hideActivity()
                      if  let json = String(data: data, encoding: String.Encoding.utf8){
                          print("Response: \(json)")
                          if let data = Mapper<TokenDataModel>().map(JSONString: json){
                            self.updateTokenAPI(refresh_token: data.refresh_token!)
                          }
                         
                      }
                      
                  }
              }
          }
    func updateTokenAPI(refresh_token: String) {
               self.showActivity()
        let params: Parameters = ["grant_type":"refresh_token","client_id":"savingps-e3e3d9","client_secret":"45894c7e-2a21-45cc-a099-5b14b8bd6527","refresh_token":refresh_token]
               Alamofire.request("https://auth.truelayer.com/connect/token" , method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON { (response) in
                   if let data = response.data {
                       self.hideActivity()
                       if  let json = String(data: data, encoding: String.Encoding.utf8){
                           print("Response: \(json)")
                           if let data = Mapper<TokenDataModel>().map(JSONString: json){
                            self.listTransactionAPI(token: data.access_token!)
                           }
                          
                       }
                       
                   }
               }
           }
    
    func listTransactionAPI(token: String) {
                  self.showActivity()
        let params: Parameters = [:]
       let accountId = "56c7b029e0f8ec5a2334fb0ffc2fface"
        
        let headers = [
           "Authorization":"Bearer \(token)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]

        Alamofire.request("https://api.truelayer.com/data/v1/accounts/\(accountId)/transactions", method: .get, parameters: params, encoding: URLEncoding.httpBody, headers: headers)
        .responseJSON { response in
                      if let data = response.data {
                          self.hideActivity()
                          if  let json = String(data: data, encoding: String.Encoding.utf8){
                              print("Response: \(json)")
                            self.responseLabel.text = json
                              if let data = Mapper<AccountTransactionResponseModel>().map(JSONString: json){
                                self.transactionResponse = data
                                self.sendTransactionDataAPI()
                                
                              }
                             
                          }
                          
                      }
                  }
              }
    
    
    func sendTransactionDataAPI() {
                     self.showActivity()
        let params: Parameters = self.transactionResponse.toJSON()
         
   

           Alamofire.request("https://django-on-cloudrun-5e57zd7bgq-ew.a.run.app/myapi/" , method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (response) in
                         if let data = response.data {
                             self.hideActivity()
                             if  let json = String(data: data, encoding: String.Encoding.utf8){
                                 print("Response: \(json)")
                               self.responseLabel.text = json
                                 if let data = Mapper<AccountTransactionResponseModel>().map(JSONString: json){
                                  
                                
                                   
                                 }
                                
                             }
                             
                         }
                     }
                 }
    
}



