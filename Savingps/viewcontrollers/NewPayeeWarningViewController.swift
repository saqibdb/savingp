//
//  NewPayeeWarningViewController.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import UIKit

class NewPayeeWarningViewController: UIViewController {
    @IBOutlet weak var warningView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.layer.cornerRadius = 25
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]

        shadowSetting(view: warningView)
    }
    
    func shadowSetting(view: UIView) {
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 4
    }
    

    @IBAction func backButtonClick(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addButtonClick(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)

    }
    
}
