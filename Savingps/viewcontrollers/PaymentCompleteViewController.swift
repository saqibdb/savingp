//
//  PaymentCompleteViewController.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import UIKit

class PaymentCompleteViewController: UIViewController {
    
@IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var attentionView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.layer.cornerRadius = 25
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
        shadowSetting(view: attentionView)

        // Do any additional setup after loading the view.
    }
    
    func shadowSetting(view: UIView) {
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 4
    }
    
    @IBAction func doneButtonClick(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)

    }
    
}
