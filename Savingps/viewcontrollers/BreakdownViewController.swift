//
//  BreakdownViewController.swift
//  Savingp's
//
//  Created by MacBook Pro on 09/01/2020.
//  Copyright © 2020 Vivek Dogra. All rights reserved.
//

import UIKit

class BreakdownViewController: UIViewController {
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var breakdownTableView: UITableView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.layer.cornerRadius = 25
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
        
        self.breakdownTableView.dataSource = self
        self.breakdownTableView.delegate = self

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BreakdownViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BreakdownTableViewCell") as! BreakdownTableViewCell

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "BreakdownDetailsViewController") as! BreakdownDetailsViewController
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    
    
}
