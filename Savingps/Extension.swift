//
//  Extension.swift
//  Savingps
//
//  Created by Vivek Dogra on 30/01/20.
//  Copyright © 2020 Vivek Dogra. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

enum AlertAction :String{
    case Ok
    case Cancel
    case Yes
    case No
    case Login
    case SignUp
}

extension UIViewController{
    
    
    func showAlert(with message:String?) {
        UIAlertController.show(title: nil, message: message, target: self, handler: nil)
    }
     func showActivity() {
           SVProgressHUD.setForegroundColor(UIColor.white)
           SVProgressHUD.setBackgroundColor(UIColor(red:74/255, green:87/255, blue:205/255, alpha: 1))
           
           SVProgressHUD.show()
           
           return
       }
        func hideActivity() {
           SVProgressHUD.dismiss()
           return
       }
    
   
}

extension UIAlertController {
    
    
   
    
    class func show(title: String?, message: String?, target:UIViewController?,actions: [AlertAction]?,preferredStyle:UIAlertController.Style?,handler: ((AlertAction) -> ())?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle!)
        if let array = actions,array.count>0 {
            for ac in array {
                alertController.addAction(UIAlertAction(title: ac.rawValue, style: .default) { action -> Void in
                    handler?(ac)
                })
            }
            
        } else {
            alertController.addAction(UIAlertAction(title: AlertAction.Ok.rawValue, style: .cancel) { action -> Void in
                handler?(AlertAction.Ok)
            })
        }
        target?.present(alertController, animated: true, completion: nil)
        
    }
    
    class func show(title: String?, message: String?, target:UIViewController?,handler: ((AlertAction) -> ())?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "ok", style: .cancel) { action -> Void in
            handler?(AlertAction.Ok)
        })
        target?.present(alertController, animated: true, completion: nil)
    }
    
    
    class func showToast(title: String?, message: String?, target:UIViewController?,handler: ((AlertAction) -> ())?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        target?.present(alertController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0, execute: {
            
            alertController.dismiss(animated: true, completion: nil)
        })
    }
}
