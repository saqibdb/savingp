//
//  Constants.swift
//  Savingp's
//
//  Created by Vivek Dogra on 22/11/19.
//  Copyright © 2019 Vivek Dogra. All rights reserved.
//

import Foundation
import Foundation
import UIKit

enum Color {
    
    static var base_color = UIColor(red: 74/255, green: 87/255, blue: 205/255, alpha: 1.0)
    static var tab_unselected_color = UIColor(red: 158/255, green: 161/255, blue: 202/255, alpha: 1.0)
    static var selected_color = UIColor(red: 55/255, green: 63/255, blue: 163/255, alpha: 1.0)
    static var barBackground = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
    
}
extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
      /* The color of the shadow. Defaults to opaque black. Colors created
       * from patterns are currently NOT supported. Animatable. */
      @IBInspectable var shadowColor: UIColor? {
          set {
              layer.shadowColor = newValue!.cgColor
          }
          get {
              if let color = layer.shadowColor {
                  return UIColor(cgColor:color)
              }
              else {
                  return nil
              }
          }
      }
      
      /* The opacity of the shadow. Defaults to 0. Specifying a value outside the
       * [0,1] range will give undefined results. Animatable. */
      @IBInspectable var shadowOpacity: Float {
          set {
              layer.shadowOpacity = newValue
          }
          get {
              return layer.shadowOpacity
          }
      }
      
      /* The shadow offset. Defaults to (0, -3). Animatable. */
      @IBInspectable var shadowOffset: CGPoint {
          set {
              layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
          }
          get {
              return CGPoint(x: layer.shadowOffset.width, y:layer.shadowOffset.height)
          }
      }
      
      /* The blur radius used to create the shadow. Defaults to 3. Animatable. */
      @IBInspectable var shadowRadius: CGFloat {
          set {
              layer.shadowRadius = newValue
          }
          get {
              return layer.shadowRadius
          }
      }
      /// Flip view horizontally.
      func flipX() {
          transform = CGAffineTransform(scaleX: -transform.a, y: transform.d)
      }
      
      /// Flip view vertically.
      func flipY() {
          transform = CGAffineTransform(scaleX: transform.a, y: -transform.d)
      }
}
