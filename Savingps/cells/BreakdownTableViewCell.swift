//
//  BreakdownTableViewCell.swift
//  Savingp's
//
//  Created by MacBook Pro on 09/01/2020.
//  Copyright © 2020 Vivek Dogra. All rights reserved.
//

import UIKit

class BreakdownTableViewCell: UITableViewCell {

    
    @IBOutlet weak var mainImageview: UIImageView!
    
    @IBOutlet weak var mainTitle: UILabel!
    
    @IBOutlet weak var mainProgressView: UIProgressView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
