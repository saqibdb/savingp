/*
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct AccountTransactionResponseModel : Mappable {
    var results : [Results]?
    var status : String?

    init?(map: Map) {

    }
    init() {
        
    }
    mutating func mapping(map: Map) {

        results <- map["results"]
        status <- map["status"]
    }

}


struct Meta : Mappable {
    var provider_transaction_category : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        provider_transaction_category <- map["provider_transaction_category"]
    }

}


struct Results : Mappable {
    var timestamp : String?
    var description : String?
    var transaction_type : String?
    var transaction_category : String?
    var transaction_classification : [String]?
    var amount : Double?
    var currency : String?
    var transaction_id : String?
    var running_balance : Running_balance?
    var meta : Meta?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        timestamp <- map["timestamp"]
        description <- map["description"]
        transaction_type <- map["transaction_type"]
        transaction_category <- map["transaction_category"]
        transaction_classification <- map["transaction_classification"]
        amount <- map["amount"]
        currency <- map["currency"]
        transaction_id <- map["transaction_id"]
        running_balance <- map["running_balance"]
        meta <- map["meta"]
    }

}

struct Running_balance : Mappable {
    var currency : String?
    var amount : Double?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        currency <- map["currency"]
        amount <- map["amount"]
    }

}
